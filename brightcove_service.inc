<?php
/**
 * @file Contain brightcove service method to submit/upload video file to brightcove media server. 
 */

function brightcove_service_upload($file, $video_meta) {
  
  $file = (object) $file;
  
  if (!isset($file->file)) {
    return FALSE;
  }
  
  $update = array();
  if (!empty($file->fid)) {
    $update = 'fid';
  }
  
  $dir = dirname($file->filepath);
  
  if (!file_check_directory($dir, FILE_CREATE_DIRECTORY)) {
    return services_error("Could not create destination directory for file.");
  }
  
  $file->filepath = file_destination(file_create_path($file->filepath), FILE_EXISTS_RENAME);
  
  if ((!isset($file->filename)) || (empty($file->filename))) {
    $file->filename = trim(basename($file->filepath), '.');
  }
  $file->filename = file_munge_filename($file->filename, $extensions, TRUE);
  $file->filemime = file_get_mimetype($file->filename);
  
  if (preg_match('/\.(php|pl|py|cgi|asp|js)$/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
    $file->filemime = 'text/plain';
    $file->filepath .= '.txt';
    $file->filename .= '.txt';
  }
  
  if (empty($file->filepath) || file_check_path($file->filepath) === FALSE) {
    return services_error("Destintion directory does not exist or is not writeable.");
  }
  
  $file->filepath .= '/'. $file->filename;
  
  if (!file_save_data(base64_decode($file->file), $file->filepath)) {
    return services_error("Could not write file to destination");
  }
  
  drupal_write_record('files', $file, $update);
  
  if (empty($update)) {
    foreach (module_implements('file_insert') as $module) {
      $function =  $module .'_file_insert';
      $function($file);
    }
  }
  
  $file->file = "";
  if (!empty($user->uid)) {
    $file->uid = $user->uid;
  }
  else {
    $file->uid = 1;
  }
  
  $read_token = variable_get('brightcove_read_api_key', '');
  $write_token = variable_get('brightcove_write_api_key', '');
  
  if (empty($read_token)) { 
    drupal_set_message(t('Cannot initialize Brightcove API. Contact site administrators.'), 'error');
    watchdog('brightcove', 'Brightcove Read API keys not found, cannot initialize Echove SDK.', array(), WATCHDOG_ERROR);
  } 
  
  else {
    $meta = array(
      'name' => $video_meta->title,
      'shortDescription' => $video_meta->shortDescription,
      'longDescription' => $video_meta->longDescription,
      'linkText' => '',
      'linkURL' => '',
      'referenceId' => brightcove_generate_reference(),
    );
    
    if (!empty($video_meta->tags)) {
      $meta['tags']= 'test,test121212';
    }
    
    $vid = brightcove_upload_video($file->filepath, $meta);
    
    $_SESSION['video_' . $vid] = $video;
    
    
    db_query('DELETE FROM {files} WHERE fid = %d', $file->fid);
    unlink($file->filepath);
    
    return $vid;
  }
}


function brightcove_service_get($vid) {
  return brightcove_video_load($vid);
}

function brightcove_service_access($nid) {
  if (user_access('brightcove service access')) {
    return TRUE;
  }
  return FALSE;
}
